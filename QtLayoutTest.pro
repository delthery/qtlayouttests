#-------------------------------------------------
#
# Project created by QtCreator 2015-12-23T17:09:57
#
#-------------------------------------------------

QT       += core gui quick quickwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtLayoutTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    traceallocator.cpp \
    channelframe.cpp

HEADERS  += mainwindow.h \
    traceallocator.h \
    channelframe.h

FORMS    += mainwindow.ui \
    tracedialog.ui \
    channelframe.ui

DISTFILES += \
    QuickUIForm.ui.qml \
    QuickUI.qml

RESOURCES += \
    mainresource.qrc
