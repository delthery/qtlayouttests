import QtQuick 2.4

Item {
    id: item1
    width: 400
    height: 400

    Rectangle {
        id: rectangle1
        width: 93
        anchors.left: parent.left
        anchors.leftMargin: 28
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 288
        anchors.top: parent.top
        anchors.topMargin: 38
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }

            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }

    Rectangle {
        id: rectangle2
        anchors.right: parent.right
        anchors.rightMargin: 31
        anchors.left: parent.left
        anchors.leftMargin: 169
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 142
        anchors.top: parent.top
        anchors.topMargin: 58
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }

            GradientStop {
                position: 1
                color: "#000000"
            }
        }
        border.color: "#e53030"
    }
}

