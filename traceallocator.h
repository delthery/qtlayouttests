#ifndef TRACEALLOCATOR_H
#define TRACEALLOCATOR_H

#include <QObject>

/*
enum chartType
{
    cartesian,
    polar,
    volpert
};

enum propType
{
    S11,
    S12,
    S22,
    S21
};

enum measType
{
    Amp,
    Phase,
    GD,
    SAR
};

class Trace
{
public:
    Trace(chartType c, propType p, measType m):
        cType(c), pType(p), mType(m)
    {
    }

    chartType   cType;
    propType    pType;
    measType    mType;
};

class TraceAllocator : public QObject
{
    Q_OBJECT
public:
    explicit TraceAllocator(QObject *parent = 0);
    //TraceAllocator(const TraceAllocator &);
    ~TraceAllocator();

signals:

public slots:
    void AddTrace(Trace* trace)
    {
//        std::get<0>(_tree[trace->cType])[trace->pType][trace->mType] = trace;
//        std::get<1>(_tree[trace->cType])[trace->mType][trace->pType] = trace;
    }

    void remTrace(Trace* trace)
    {
        //std::get<0>(_tree[trace->cType])[trace->pType][trace->mType].remove();
        //std::get<1>(_tree[trace->cType])[trace->mType][trace->pType].remove();
    }

private:    

    std::map<
        chartType,
        std::tuple<
            std::map<
                propType,
                std::map<measType, Trace*>
            >,
            std::map<
                measType,
                std::map<propType, Trace*>
            >,
        >
    > _tree;

    QMap<chartType,
        QPair<
            QMap<propType,
                QMap<measType,Trace*>>,
            QMap<measType,
                QMap<propType,Trace*>>>> _qTree;
};
*/
#endif // TRACEALLOCATOR_H
