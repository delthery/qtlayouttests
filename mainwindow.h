#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_2_clicked();

    void on_comboBox_activated(int index);

    void on_actionAddDock_triggered();

    void on_actionRemoveDock_triggered();

    void on_actionAddTab_triggered();

    void on_actionRemoveTab_triggered();

private:
    Ui::MainWindow *ui;
    QTabWidget*         _tab;
    QList<QMainWindow*> _tabWindows;
    QList<QList<QDockWidget*>> _docks;
};

#endif // MAINWINDOW_H
