#include "mainwindow.h"
#include <QApplication>
#include "traceallocator.h"
#include <QStyleFactory>

int main(int argc, char *argv[])
{
/*    TraceAllocator ta;

    ta.AddTrace(new Trace(chartType::cartesian, propType::S11, measType::Amp));
    ta.AddTrace(new Trace(chartType::cartesian, propType::S11, measType::Phase));
    ta.AddTrace(new Trace(chartType::cartesian, propType::S12, measType::Amp));
    ta.AddTrace(new Trace(chartType::cartesian, propType::S12, measType::Phase));
    ta.AddTrace(new Trace(chartType::cartesian, propType::S22, measType::Amp));
    ta.AddTrace(new Trace(chartType::cartesian, propType::S22, measType::Phase));

    ta.AddTrace(new Trace(chartType::polar, propType::S11, measType::GD));
    ta.AddTrace(new Trace(chartType::polar, propType::S12, measType::GD));
    ta.AddTrace(new Trace(chartType::polar, propType::S22, measType::GD));
    ta.AddTrace(new Trace(chartType::polar, propType::S22, measType::SAR));
*/
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    QApplication::setStyle(QStyleFactory::create("Fusion"));
    QPalette p;
    p = qApp->palette();
    p.setColor(QPalette::Window, QColor(53,53,53));
    p.setColor(QPalette::Button, QColor(53,53,53));
    p.setColor(QPalette::Highlight, QColor(142,45,197));
    p.setColor(QPalette::ButtonText, QColor(255,255,255));
    qApp->setPalette(p);

    w.setStyleSheet(
        "QMainWindow::separator {\
            background: rgb(200, 200, 200);\
            width: 2px;\
            height: 2px;\
        }\
        \
        QTabBar::close-button {\
            image: url(:/new/icons10/resources/ico10/cancel_104px.png)}");
    return a.exec();
}
