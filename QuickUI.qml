import QtQuick 2.4

QuickUIForm {
    id: quickUIForm1
    GridView {
        id: gridView1
        anchors.top: parent.top
        anchors.topMargin: 26
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 31
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.rightMargin: 252
        anchors.leftMargin: 8
        delegate: Item {
            x: 5
            height: 50
            Column {
                Rectangle {
                    width: 40
                    height: 40
                    color: colorCode
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Text {
                    x: 5
                    text: name
                    font.bold: true
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                spacing: 5
            }
        }
        cellWidth: 70
        model: ListModel {
            ListElement {
                name: "Grey"
                colorCode: "grey"
            }

            ListElement {
                name: "Red"
                colorCode: "red"
            }

            ListElement {
                name: "Blue"
                colorCode: "blue"
            }

            ListElement {
                name: "Green"
                colorCode: "green"
            }
        }
        cellHeight: 70
    }
}

