#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDockWidget>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //setCentralWidget(new QWidget());

    _tab = new QTabWidget(this);
    _tab->setTabsClosable(true);
    //!!TODO
    setCentralWidget(_tab);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_2_clicked()
{
    int index0 =  ui->comboBox->currentIndex();

    int index = ui->comboBox->findText("value1");
    ui->label->setText(QString("%1->%2").arg(
                           QString::number(index0),
                           QString::number(index)));
    if(index!=-1)
    {
        ui->comboBox->setCurrentIndex(index);
    }
}

void MainWindow::on_comboBox_activated(int index)
{

}

void MainWindow::on_actionAddDock_triggered()
{
    int index = _tab->currentIndex();

    QDockWidget* prevDock = nullptr;

    if(_docks[index].size() > 1)
        prevDock = _docks[index].last();

    QMainWindow* win = _tabWindows[index];

    QDockWidget* dock = new QDockWidget(
                QString("Dock %1").arg(_docks[index].size()),
                win);
    //dock->setFeatures(QDockWidget::DockWidgetVerticalTitleBar|QDockWidget::AllDockWidgetFeatures);
    //dock->setMinimumWidth(dock->parentWidget()->width() / (_docks[index].size()+1));
    //dock->setAttribute(Qt::WA_WState_ExplicitShowHide);
    QWidget* dummy = new QWidget;
    //dummy->resize(QSize(50,10));
    //dummy->setCursor(QCursor::);
    QLabel* label = new QLabel(this);
    label->setText("Label");
    //dock->setTitleBarWidget(label);

    QFrame* frame = new QFrame(dock);
    dock->setWidget(frame);

    switch(_docks[index].size())
    {
    case 0:
        win->addDockWidget(Qt::TopDockWidgetArea, dock);
        break;
    case 1:
        //win->addDockWidget(Qt::BottomDockWidgetArea, dock);
        win->splitDockWidget(_docks[index][0], dock, Qt::Vertical);
        break;
    case 2:
        win->splitDockWidget(_docks[index][0], dock, Qt::Horizontal);
        break;
    case 3:
        win->splitDockWidget(_docks[index][1], dock, Qt::Horizontal);
        break;
    case 4:
        //win->addDockWidget(Qt::BottomDockWidgetArea, dock);
        //win->splitDockWidget(Qt::BottomDockWidgetArea, dock, Qt::Vertical);
        //win->splitDockWidget(_docks[index][1], dock, Qt::Vertical);
        //win->splitDockWidget((QDockWidget*)win, dock, Qt::Vertical);
        win->addDockWidget(Qt::TopDockWidgetArea, dock, Qt::Vertical);
        break;
    case 5:
        win->splitDockWidget(_docks[index][4], dock, Qt::Horizontal);
        break;
    case 6:
        win->splitDockWidget(_docks[index][2], dock, Qt::Horizontal);
        break;
    case 7:
        win->splitDockWidget(_docks[index][3], dock, Qt::Horizontal);
        break;
    case 8:
        win->splitDockWidget(_docks[index][5], dock, Qt::Horizontal);
        break;
    default:
        break;
    }

/*
    if(_docks[index].size() % 2 == 0)
    {
        if(prevDock == 0)
        {
            win->addDockWidget(Qt::TopDockWidgetArea, dock);
            qDebug() << "Add 1";
        }
        else //if(prevDock != 0)
        {
            win->splitDockWidget(prevDock, dock, Qt::Horizontal);
            qDebug() << "Split 1";
        }
    }
    else
    {
        if(prevDock == 0)
        {
            win->addDockWidget(Qt::BottomDockWidgetArea, dock);
            qDebug() << "Add 2";
        }
        else //if(prevDock!=0)
        {
            win->splitDockWidget(prevDock, dock, Qt::Vertical);
            qDebug() << "Split 2";
        }
    }
*/
    _docks[index].append(dock);

    win->layout()->invalidate();
    for(auto*child:win->findChildren<QWidget*>())
    {
        child->updateGeometry();
    }
    win->updateGeometry();

    //int width = win->width() / ((_docks[index].size()-1)/2+1);
    //frame->resize(QSize(width,frame->height()));

    if(_docks[index].size() > 0)
        ui->actionRemoveDock->setDisabled(false);

    qDebug() << QString("%1 docks").arg(_docks.size());
    //if(prevDock != 0)
      //  win->splitDockWidget(prevDock, dock, Qt::Horizontal);


}

void MainWindow::on_actionRemoveDock_triggered()
{    
    int index = _tab->currentIndex();
    QMainWindow* win = _tabWindows[index];

    QDockWidget* dock = _docks[index].last();
    removeDockWidget(dock);
    _docks[index].removeLast();

    qDebug() << QString("%1 docks").arg(_docks[index].size());

    if(_docks[index].size() == 0)
    {
        ui->actionRemoveDock->setDisabled(true);
        qDebug() << "Remove Dock Disabled";
    }

}

void MainWindow::on_actionAddTab_triggered()
{
    QMainWindow* win = new QMainWindow(this);
    win->setDockNestingEnabled(true);
    win->setDockOptions(win->dockOptions() & (~QMainWindow::AllowTabbedDocks | QMainWindow::AllowNestedDocks));

    int index = _tab->addTab(
                qobject_cast<QWidget*>(win),
                QString("Tab %1").arg(_tabWindows.size()));
    _tab->setCurrentIndex(index);
    _tabWindows.append(win);
    QList<QDockWidget*> list;
    _docks.append(list);
}

void MainWindow::on_actionRemoveTab_triggered()
{
    int index = _tab->currentIndex();

    _tab->removeTab(index);
    _tabWindows.removeAt(index);
    _docks.removeAt(index);
}
